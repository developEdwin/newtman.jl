# Newtman

[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)
[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://developEdwin.gitlab.io/Newtman.jl/dev)
[![Build Status](https://travis-ci.com/developEdwin/Newtman.jl.svg?branch=master)](https://travis-ci.com/developEdwin/Newtman.jl)
[![Build Status](https://ci.appveyor.com/api/projects/status/github/developEdwin/Newtman.jl?svg=true)](https://ci.appveyor.com/project/developEdwin/Newtman-jl)
[![Build Status](https://gitlab.com/developEdwin/Newtman.jl/badges/master/build.svg)](https://gitlab.com/developEdwin/Newtman.jl/pipelines)
[![Coverage](https://gitlab.com/developEdwin/Newtman.jl/badges/master/coverage.svg)](https://gitlab.com/developEdwin/Newtman.jl/commits/master)
[![Codecov](https://codecov.io/gh/developEdwin/Newtman.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/developEdwin/Newtman.jl)
[![Coveralls](https://coveralls.io/repos/github/developEdwin/Newtman.jl/badge.svg?branch=master)](https://coveralls.io/github/developEdwin/Newtman.jl?branch=master)
